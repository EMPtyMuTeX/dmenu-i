# dmenu-i: A dmenu improvement script

## About

_Author: EMPty_MuTeX_

_Release: 1.0_

_License: GNU GPLv3_

This script improves dmenu the following ways:
* Allows selecting where to run stuff.
* Asks if you really want to poweroff/reboot the system.
* Has a simple config file.

## Requirements
The required packages are the following:
* `dmenu`: This script is not a replacement for dmenu. It is a script to dodge some drawbacks from the default script, dmenu_run.
* `i3`: This script has been build to run on the i3wm, if you can adapt it to other tiling window managers, feel free to copy this project and adapt it.
* `make`: Used to install the thing.

## Optional packages
These are optional packages, they may not be necessary, but they provide the best experience:
* `newt`: Provides whiptail command, used by dmenu-i-wizard. This package is optional, dmenu-i-wizard has a basic question-answer configuration system now.
* `xfce4-terminal`: The default terminal emulator used in this script. You can always modify the script to make it work with other terminal emulators.

## Configuration
To configure dmenu-i, the recommended way is to use the dmenu-i-wizard script.
If you wish to configure it manually, the variables to set up in "$HOME/.config/dmenu-i/config" are:
* `color_sb`: The background color for selections. Eg: `color_sb=#6a0090`.
* `font`: The font to use. Eg:`font=Inconsolata-13:Regular`.
* `term`: The terminal emulator to use for the **holdterm** and **terminal** running modes. Eg: `term=xfce4-terminal`.

## Testing
This script has been tested on a Fedora (FC31) Server setup, running i3wm as a tiling window manager. I don't really know if it will work in other distros running i3, feel free to test it out, modify it, ect.

## Installation
Simply run: `sudo make install`. To install it with somewhat more strict permissions, run: `sudo make install_secure`.

## Usage
Simply run `dmenu-i` and the program will run like dmenu_run does.

The program has the capacity to detect if the config file exists, and if that condition is not met, it will run the wizard in a terminal. This feature only works with `i3-sensible-terminal`.

Whenever you run a command, it will show the following options:
* **Background**: Runs the command in background and disowns it, like dmenu_run would do.
* **Holdterm**: Runs the command in the terminal-emulator specified in the configuration file, with the `--hold` argument. This only works in some terminal-emulators, and may need adaptation for some terminal-emulators. Currently working on `xfce4-terminal`.
* **Terminal**: Runs the command in the terminal-emulator specified in the configuration file. This should work in most terminal-emulators.
* **Cancel**: You made a mistake and don't want to run that? No problem, select this option.

## Uninstallation
Run the following: `sudo make uninstall`. **NOTE**: Running the `uninstall` make function won't remove user configuration! That has to be removed manually.
