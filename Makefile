# Makefile
# This file installs and uninstalls dmenu-i from your system.
# This Makefile has the following functions:
#  	- install: Installs the script in $TARGET ("/usr/local/bin" or the set bin path if you modify this) with standard permissions. Eg: user.user(rwxr-xr-x).
# 	- install_secure: Installs the script in $TARGET ("/usr/local/bin" or the set bin path if you modify this) with strict permissions, the minimum required to run the program,
# 					  but not allowing anyone without root permissions to overwrite the file. Eg: root.root(r-xr-xr-x) [RECOMMENDED MODE]
# 	- uninstall: Uninstalls the program from the folder in $TARGET.

# Command for copying to $TARGET
CHMOD = chmod
CHOWN = chown
CP = cp
RM = rm

# Args
CHMOD_ARGS= +x
CHMOD_ARGS_SEC= 555
CHOWN_ARGS= root:root

# Target
TARGET = /usr/local/bin/

FILES = {dmenu-i,dmenu-i-wizard}

all:
			$(info Please run one of the following make targets: install, install_secure, uninstall)

install:
			$(info ***** Installing *****)
			$(CP) $(FILES) $(TARGET)
			$(CHMOD) $(CHMOD_ARGS) $(TARGET)$(FILES)
install_secure:
			$(info ***** Installing with secure file ownerships *****)
			$(CP) $(FILES) $(TARGET)
			$(CHMOD) $(CHMOD_ARGS_SEC) $(TARGET)$(FILES)
			$(CHOWN) $(CHOWN_ARGS) $(TARGET)$(FILES)
uninstall:
			$(info ***** Uninstalling *****)
			$(RM) $(TARGET)$(FILES)
